/*-
 * Copyright (c) 2008  Peter Pentchev
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 */

#include <ctype.h>
#include <err.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "timewrap.h"

static const char	*conffile = "timewrap.conf";

static struct timewrap	*wrap;
static size_t		 wrapcnt;

static int	 checkconf(struct timewrap *, size_t);
static int	 freeconf(struct timewrap *, size_t *);
static int	 readconf(const char *);
static void	 usage(int);
static int	 xwordexp(const char *, char ***, size_t *);

int
main(int argc, char *argv[])
{
	int r;
	size_t i, j;
	int nargc, k;
	char **nargv;

	if (argc < 1)
		usage(1);

	r = readconf(conffile);
	RDBG(("readconf returned %d\n", r));
	if (r != 0)
		return (r);
	r = checkconf(wrap, wrapcnt);
	RDBG(("checkconf returned %d\n", r));
	if (r != 0)
		return (r);

	RDBG(("Searching for a section '%s'\n", argv[0]));
	for (i = 0; i < wrapcnt; i++)
		if (!strcmp(argv[0], wrap[i].label))
			break;
	if (i == wrapcnt)
		errx(1, "No configuration label for '%s'", argv[0]);
	RDBG(("Found, '%s' with %u args\n", wrap[i].progname, wrap[i].argcnt));

	/* Build the new argv */
	nargc = wrap[i].argcnt + argc;
	nargv = malloc((nargc + 1) * sizeof(*nargv));
	nargv[0] = wrap[i].progname;
	for (j = 0; j < wrap[i].argcnt; j++)
		nargv[j + 1] = wrap[i].args[j];
	for (k = 1; k < argc; k++)
		nargv[j + k] = argv[k];
	nargv[j + k] = NULL;
#ifdef EBUG
	RDBG(("About to execute '%s' with %u arguments...", nargv[0], j + k));
	for (k = 0; k < nargc + 1; k++)
		RDBG((" '%s'", (nargv[k] != NULL? nargv[k]: "(null)")));
	RDBG(("...done\n"));
#endif
	execv(nargv[0], nargv);
	err(1, "Executing the %s program", nargv[0]);
}

/**
 * Function:
 *	freeconf	- free a configuration array
 * Inputs:
 *	conf		- the configuration to free
 *	cnt		- val/res the number of configuration items to free
 * Returns:
 *	0 on success, no errors so far.
 * Modifies:
 *	Frees up and zeroes the contents of the conf array.
 *	Stores a zero into the cnt pointer.
 */
static int
freeconf(struct timewrap *conf, size_t *cnt)
{
	size_t i, j;

	if (conf == NULL) {
		if (cnt != NULL && *cnt > 0) {
			fprintf(stderr,
			    "Internal error: no conf, count %u in readconf\n",
			    *cnt);
			exit(1);
		}
		return (0);
	}
	if (cnt == NULL) {
		fprintf(stderr, "Internal error: null cnt in freeconf\n");
		exit(1);
	}

	for (i = 0; i < *cnt; i++) {
		if (conf[i].label != NULL)
			free(conf[i].label);
		if (conf[i].progname != NULL)
			free(conf[i].progname);
		if (conf[i].args != NULL) {
			for (j = 0; conf[i].args[j] != NULL; j++)
				free(conf[i].args[j]);
			bzero(conf[i].args, j * sizeof(conf[i].args[0]));
			free(conf[i].args);
		}
	}
	bzero(conf, i * sizeof(conf[0]));
	free(conf);
	return (0);
}

/*
 * Function:
 *	checkconf	- validate the data read from the config file
 * Inputs:
 *	conf		- the config data to validate
 *	cnt		- the number of the config items
 * Returns:
 *	0 on success, so far always 1 on error.
 * Modifies:
 *	Nothing; may write to stderr.
 */
static int
checkconf(struct timewrap *conf, size_t cnt)
{
	size_t errs, i;

	if (cnt == 0) {
		fprintf(stderr, "No configuration items defined\n");
		return (1);
	}

	for (i = errs = 0; i < cnt; i++) {
		if (conf[i].label == NULL || conf[i].label[0] == '\0') {
			fprintf(stderr, "Config item %u: no label\n", i + 1);
			errs++;
			continue;
		}
		if (conf[i].progname == NULL || conf[i].progname == '\0') {
			fprintf(stderr, "Config item %u '%s': no progname\n",
			    i + 1, conf[i].label);
			errs++;
		}
		if (conf[i].args == NULL) {
			fprintf(stderr, "Config item %u '%s': no args\n",
			    i + 1, conf[i].label);
			errs++;
		}
	}
	if (errs > 0) {
		fprintf(stderr, "%u configuration error%s\n", errs,
		    (errs != 1? "s": ""));
		return (1);
	}
	return (0);
}

/**
 * Function:
 *	readconf	- read the timewrap configuration from a file
 * Inputs:
 *	fname		- the name of the config file to read
 * Returns:
 *	0 on success, for the present 1 on error.
 * Modifies:
 *	The wrap array and the wrapcnt variable.
 */
static int
readconf(const char *fname)
{
	FILE *fp;
	size_t i, all, nall, linenum, len, errs, started;
	struct timewrap *conf, *nconf;
	char line[256];
	char *cur, *last, *end, *var, *val;

	if (fname == NULL) {
		fprintf(stderr, "Internal error: null fname in readconf\n");
		return (1);
	}
	RDBG(("readconf(%s) started\n", fname));
	fp = fopen(fname, "rt");
	if (fp == NULL) {
		fprintf(stderr, "Could not open the configuration file %s\n",
		    fname);
		return (1);
	}

	all = i = linenum = errs = started = 0;
	conf = NULL;
	while (fgets(line, sizeof(line), fp) != NULL) {
		linenum++;
		len = strlen(line);
		if (len > 0 && line[len - 1] != '\n') {
			fprintf(stderr, "Line too long, line %u file %s\n",
			    linenum, fname);
			errs++;
			break;
		}
		while (len > 0 &&
		    (line[len - 1] == '\r' || line[len - 1] == '\n'))
			line[--len] = '\0';
		RDBG(("read line %u: %s\n", linenum, line));
		if (len == 0 || line[0] == '#')
			continue;

		/* Empty line? */
		cur = line;
		while (isspace(*cur))
			cur++;
		if (*cur == '\0')
			continue;

		/* Section header? */
		if (*cur == '[') {
			RDBG(("section header..."));
			/* Is this a correct section name? */
			cur++;
			while (isspace(*cur))
				cur++;
			if (*cur == '\0') {
				fprintf(stderr, "Line %u: no label name: %s\n",
				    linenum, line);
				errs++;
				break;
			}
			last = strchr(cur + 1, ']');
			if (last == NULL) {
				fprintf(stderr,
				    "Line %u: no closing ']' in label: %s",
				    linenum, line);
				errs++;
				break;
			}
			*last = '\0';
			RDBG((" name '%s'", cur));
			end = last + 1;
			while (isspace(*end))
				end++;
			if (*end != '\0') {
				fprintf(stderr,
				    "Line %u: extra data after label: %s\n",
				    linenum, line);
				errs++;
				break;
			}
			while (last > cur + 1 && isspace(last[-1]))
				last--;
			if (last == cur + 1) {
				fprintf(stderr, "Line %u: no label name: %s\n",
				    linenum, line);
				errs++;
				break;
			}

			/* Start a new config item, reallocating if needed */
			RDBG((" check-all(%u, %u)", i, all));
			if (i + 1 >= all) {
				nall = all * 2 + 1;
				nconf = realloc(conf, nall * sizeof(*conf));
				RDBG((" realloc'd(%u, %p)", nall, (void *)nconf));
				if (nconf == NULL) {
					fprintf(stderr,
					    "No memory for %u config items\n",
					    nall);
					errs++;
					break;
				}
				bzero(nconf + all,
				    (nall - all) * sizeof(*nconf));
				conf = nconf;
				all = nall;
			}
			RDBG((" started(%u, %u)", started, i));
			if (started)
				i++;
			else
				started = 1;
			RDBG((" now(%u, %u)", started, i));

			conf[i].label = strdup(cur);
			RDBG((" stored!\n"));
			continue;
		}
		if (!started) {
			fprintf(stderr, "Line %u outside a section: %s\n",
			    linenum, line);
			errs++;
			break;
		}

		/* var = value? */
		var = cur;
		while (*cur != '\0' && !isspace(*cur))
			cur++;
		if (*cur == '\0') {
			fprintf(stderr, "Misformatted line %u: %s\n",
			    linenum, line);
			errs++;
			break;
		}
		/* Mark the end of the varname */
		*cur++ = '\0';
		if (isspace(*cur)) {
			*cur++ = '\0';
			while (isspace(*cur))
				cur++;
			if (*cur != '=') {
				fprintf(stderr, "Misformatted line %u: %s\n",
				    linenum, line);
				errs++;
				break;
			}
		} else {
			if (*cur != '=') {
				fprintf(stderr, "Misformatted line %u: %s\n",
				    linenum, line);
				errs++;
				break;
			}
			*cur = '\0';
		}
		cur++;
		while (isspace(*cur))
			cur++;
		/* The value starts here and ends at EOL minus whitespace */
		val = cur;
		last = cur + strlen(cur);
		while (last > cur + 1 && isspace(last[-1]))
			last--;
		/* Mark the end of the value */
		*last = '\0';

		/* Oof. */
		if (!strcmp(var, "progname")) {
			conf[i].progname = strdup(val);
		} else if (!strcmp(var, "args")) {
			if (xwordexp(val, &conf[i].args,
			    &conf[i].argcnt) != 0) {
				errs++;
				break;
			}
		} else {
			/* FIXME: implement "alias" */
			fprintf(stderr,
			    "Unknown variable '%s' in section '%s' line %u\n",
			    var, conf[i].label, linenum);
			errs++;
			break;
		}
	}
	if (errs) {
		freeconf(conf, &i);
		return (1);
	}
	if (started)
		i++;

	/* Well, well, well! */
	freeconf(wrap, &wrapcnt);
	wrap = conf;
	wrapcnt = i;
	return (0);
}

/**
 * Function:
 *	usage		- display timewrap usage information
 * Inputs:
 *	ferr		- (flag) output to stderr instead of stdout
 * Returns:
 *	Never.
 * Modifies:
 *	Nothing; writes to stdout or stderr and exits the program.
 */
static void
usage(int ferr)
{
	const char *s = "Usage: timewrap [args...]\n";

	if (ferr)
		fputs(s, stderr);
	else
		fputs(s, stdout);
	exit(ferr);
}

/**
 * Function:
 *	xwordexp	- split a string into words
 * Inputs:
 *	str		- the string to split
 *	words		- a pointer to the words array to fill in
 *	cnt		- the number of words found
 * Returns:
 *	0 on success, so far only 1 on errors.
 * Modifies:
 *	Stores an address of something into *words and the address of
 *	something else into *cnt.
 */
static int
xwordexp(const char *str, char ***words, size_t *cnt)
{
	size_t i, all, nall, inq, indq, len;
	char **w, **nw, *word, *d, *nword;
	const char *p, *start, *end;

	i = all = 0;
	w = NULL;
	p = str;
	inq = indq = 0;
	while (*p != '\0') {
		while (isspace(*p))
			p++;
		if (*p == '\0')
			break;

		start = p;
		end = NULL;
		inq = indq = 0;
		word = malloc(strlen(p) + 1);
		if (word == NULL) {
			fprintf(stderr, "Out of memory in xwordexp()\n");
			return (1);
		}
		d = word;
		while (*p != '\0' && end == NULL) {
			switch (*p) {
				case '\'':
					if (!indq)
						inq = !inq;
					else
						*d++ = *p;
					break;

				case '"':
					if (!inq)
						indq = !indq;
					else
						*d++ = *p;
					break;

				default:
					if (isspace(*p) && !(inq || indq))
						end = p;
					else
						*d++ = *p;
					break;
			}
			if (end == NULL)
				p++;
		}
		*d++ = '\0';
		if (end == NULL) {
			if (*p == '\0') {
				end = p;
			} else {
				fprintf(stderr, "Internal error: null end not at end of string in xwordexp()\n");
				return (1);
			}
		}

		len = strlen(word);
		nword = realloc(word, len + 1);
		if (nword == NULL) {
			fprintf(stderr,
			    "xwordexp: could not allocate %u bytes\n",
			    len + 1);
			return (1);
		}
		word = nword;

		if (i + 1 >= all) {
			nall = 2 * (all + 1);
			nw = realloc(w, nall * sizeof(*nw));
			if (nw == NULL) {
				fprintf(stderr, "xwordexp: out of memory\n");
				return (1);
			}
			bzero(nw + all, (nall - all) * sizeof(*nw));
			w = nw;
			all = nall;
		}
		w[i++] = word;
	}

	*words = w;
	*cnt = i;
	return (0);
}
